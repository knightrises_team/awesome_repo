#ifndef _COLORS_
#define _COLORS_
#include "plf_nanotimer.h"
/* FOREGROUND */
#define RST  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define FRED(x) KRED x RST
#define FGRN(x) KGRN x RST
#define FYEL(x) KYEL x RST
#define FBLU(x) KBLU x RST
#define FMAG(x) KMAG x RST
#define FCYN(x) KCYN x RST
#define FWHT(x) KWHT x RST

#define BOLD(x) "\x1B[1m" x RST
#define UNDL(x) "\x1B[4m" x RST

#endif  /* _COLORS_ */

#define ENABLE_LOGGER 1

#include <boost/array.hpp>

#ifdef ENABLE_LOGGER

#define LOGGER_START(ACTIVE,MIN_PRIORITY, FILE) Logger::Start(ACTIVE,MIN_PRIORITY, FILE);
#define LOGGER_STOP() Logger::Stop();
#define LOGGER_FLUSH() Logger::Flush();
#define LOGGER_WRITE(PRIORITY,LINE, MESSAGE) Logger::Write(PRIORITY ,LINE, MESSAGE);
#define LOGGER_TIMESTAMP() Logger::LocalTime() ;
#define LOGGER_TIME_IN_HUMAN_READABLE() Logger::instance.timeStampToHReadbletillns(ostream &os) ;

#else

#define LOGGER_START(MIN_PRIORITY, FILE)
#define LOGGER_STOP()
#define LOGGER_WRITE(PRIORITY ,LINE, MESSAGE)
#define LOGGER_TIMESTAMP() Logger::LocalTime() ;

#endif

#define BUF_SIZE 80

#include <string>
#include <fstream>
#include <iostream>
#include <fstream>
#include <vector>

#include "../includes/plf_nanotimer.h"

using namespace std ;


class Logger
{
public:

    const string PRIORITY_NAMES[10] =
    {
        "MKTT",
        "CNFG",
        "OUET",
        "WRNG",
        "CTOR",
        "PRMS",
        "EROR",
        "TMER",
        "PLCF",
        "INFO"
    };

    enum Priority
    {
        MKT = 0,
        CONFIG,
        OUP,
        WARNING,
        CONSTRUCT,
        PARAMS,
        ERROR,
        TIMER,
        PLCFO,
        INFO
    };

    //static char hevc[512] ;
    void Start(bool ACTIVE , Priority minPriority, const string& logFile);
    void Flush();
    void Stop();
    std::string Logger::LocalTime() ;
    void signal_handler(int sig_number) ;
    void Write(Priority priority, int LINE, const string& message);
    void Logger::timeStampToHReadbletillns(ostream &os) ;
    Logger();

private :

    bool active = false ;
    plf::nanotimer time1 ;
    uint64_t buffcounter = 0 ;
    uint64_t counter = 0 ;
    time_t timeinsec ;
    long timeinns = 0 ;
    long rawtime = 0 ;
    struct tm * dt;
    char  buf[80] = {0};
    fstream fileStream;
    struct timeval currentTime;
    char hevc[100] = {0} ;
    Priority minPriority = {0} ;
    const string LOGFILE = {0};

};

