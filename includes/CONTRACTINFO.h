#ifndef CONTRACTINFO_H
#define CONTRACTINFO_H

#include "SharedClasses.h"

/* OPTIONTYPE : CE/PE STANDS FOR EUROPEAN OPTIONS AND CA/PA STANDS FOR AMERICAN OPTIONS.
 *TO BE USED WITH FUNCTION ASKING FOR OPTIONTYPE AS AN INPUT.
 *NIFTY FUT JAN 18 HAS TOKEN NUMBER 46667
 *DATA REQUIRED FOR CE AND FOR LTP WE CAN PASS CURRENT RUNNING LTP OR THE DESIRED NUMBER WE ARE LOOKING
 *STRIKE FOR
 *NOTE : LTP MUST IN BE IN PAISE . THE RUNNING LTP YOU ARE RECIEVING IS ALWAYS IN PAISE.
 * EXAMPLE : GetATMStrikeForSecurity(46667,CE,1020000); HERE 1020000 STANDS FOR 10200 i.e; 1020000/100
 */
enum OPTIONTYPE
{
    CE=0,
    PE,
    CA,
    PA
};


class CONTRACTINFO
{
    public:
        CONTRACTINFO()=delete;

        /*USE TO GET NEXT MONTH EXPIRY FOR FUTURE CONTRACT
         * ScriptCode CAN BE FOR EXAMPLE USED AS ABOVE NIFTY FUT JAN 18 46667
         * WILL NEXT NUMERIC SCRIPT CODE FOR THE NEXT EXPIRY OF THE SECURITY*/

        static const int GetNextExpirySecurity(const int ScriptCode);///DONE

        /* USE TO GET FUTURE CONTRACT FOR THE OPTION SECURITY.
         * ScriptCode HAS TO BE OF THE OPTION TOKEN(NUMERIC)*/

        static const int GetFutureForSecurity(const int ScriptCode);///DONE

        /*USE TO GET CASH TOKEN FOR FUTURE / OPTION TOKEN
         * */
        static const int GetCashForSecurity(const int ScriptCode);///DONE

        /* USE TO GET AT_THE_MONEY STRIKE TOKEN FOR REQUESTED FUTURE TOKEN BASED ON THE LTP OR PRICE IN PAISE AND FOR OPTIONTYPE*/
        static const int GetATMStrikeForSecurity(const int ScriptCode,const OPTIONTYPE OPT, const int LTP);///DONE

        /* USE TO GET OUT_OF_THE_MONEY STRIKE TOKEN FOR REQUESTED FUTURE TOKEN BASED ON THE LTP OR PRICE IN PAISE AND FOR OPTIONTYPE*/
        static const int GetOTMStrikeForSecurity(const int ScriptCode,const OPTIONTYPE OPT, const int LTP);///DONE

        /* USE TO GET IN_TO_THE_MONEY STRIKE TOKEN FOR REQUESTED FUTURE TOKEN BASED ON THE LTP OR PRICE IN PAISE AND FOR OPTIONTYPE*/
        static const int GetITMStrikeForSecurity(const int ScriptCode,const OPTIONTYPE OPT, const int LTP);///DONE

        /* NOT IN USE AT PRESENT . */
        static const bool IsOption(const int ScriptCode);
        /* NOT IN USE AT PRESENT . */
        static const bool IsFuture(const int ScriptCode);
        /* NOT IN USE AT PRESENT . */
        static const bool IsEquity(const int ScriptCode);

        /* NOT IN USE AT PRESENT . */
        static const int GetLotSizeForSecurity(const int ScriptCode);
        /* NOT IN USE AT PRESENT . */
        static const int GetTickSizeForSecurity(const int ScriptCode);

        /* USE TO FETCH STRIKE VALUE FOR THE TOKEN. EXAMPLE : NIFTY OPTIDX CE DEC10400CE TOKEN IS 35002 . SO PASS
         * 35002 AND GET 1040000 (IN PAISE) AS RESULT*/
        static const int GetStrikeValueForSecurity(const int ScriptCode);///DONE

        /* USE TO FETCH ALTERNATE OPTION TYPE FOR ANY GIVEN OPTION TOKEN.
         * EXAMPLE : NIFTY OPTIDX CE DEC10400CE TOKEN IS 35002 . PASSING
         * 35002 WILL RETURN SCRIPTCODE FOR PE IN SAME STRIKE AND EXPIRY*/
        static const int GetAltOptionTypeForSecurity(const int ScriptCode);///DONE


        static const int GetOptionTypeForSecurity(const int ScriptCode);///DONE

        /* USE TO GET UPPER TR FOR ANY REQUESED TOKEN . IF ASKED BEFORE THE
         * SNAPSHOT DATA HAS STARTED IT WOULD RETURN 0 . AS TR IS UPDATED USING SNAPSHOT FEED.
         * */
        static const int GetUpperTRForSecurity(const int ScriptCode);///DONE

        /* USE TO GET LOWER TR FOR ANY REQUESED TOKEN . IF ASKED BEFORE THE
         * SNAPSHOT DATA HAS STARTED IT WOULD RETURN 0 . AS TR IS UPDATED USING SNAPSHOT FEED.
         * */
        static const int GetLowerTRForSecurity(const int ScriptCode);///DONE

        /* USE TO GET UPPER DPR FOR ANY REQUESED TOKEN . IF ASKED BEFORE THE
         * SNAPSHOT DATA HAS STARTED IT WOULD RETURN VALUE FROM BHAVCOPY .
         * */
        static const int GetUpperDPRForSecurity(const int ScriptCode);///DONE

        /* USE TO GET LOWER DPR FOR ANY REQUESED TOKEN . IF ASKED BEFORE THE
         * SNAPSHOT DATA HAS STARTED IT WOULD RETURN VALUE FROM BHAVCOPY .
         * */
        static const int GetLowerDPRForSecurity(const int ScriptCode);///DONE

        static const int GetLTPForSecurity(const int ScriptCode);///DONE
        //virtual ~CONTRACTINFO();
        static const int GetLTTForSecurity(const int ScriptCode);///DONE

        static const int GetLTQForSecurity(const int ScriptCode);///DONE
        static const PriceDepth GetDepthForSecurity(const int ScriptCode);
        static const int GetExpiry(const int ScriptCode);///DONE
        static const int GetInstType(const int ScriptCode);///DONE
        static const int GetNextStrike(const int ScriptCode);///DONE
        static const int GetPreviousStrike(const int ScriptCode);///DONE
    protected:

    private:
};

#endif // CONTRACTINFO_H
