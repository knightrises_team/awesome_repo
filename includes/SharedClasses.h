#ifndef SHAREDCLASSES_H_INCLUDED
#define SHAREDCLASSES_H_INCLUDED

#include "SharedHeader.h"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <vector>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/container/scoped_allocator.hpp> /// FOr nested map Depth Data
#include <vector>

#include <pthread.h>

#include <boost/asio/time_traits.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <boost/asio.hpp>
#include <boost/make_shared.hpp>

#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include <boost/ptr_container/ptr_map.hpp>
#include <unordered_map>

#include <boost/interprocess/containers/map.hpp>

#include <boost/signals2.hpp>

using namespace boost;
using namespace std;
namespace bmi = boost::multi_index;

using boost::multi_index::nth_index;
using boost::multi_index::get;
namespace bip=boost::interprocess;

#define ORDERVIEW 0
#define TOKENVIEW 1
#define PRICEVIEW 2

#define IOC 0
using namespace boost::signals2;
typedef boost::signals2::signal<void ( LOGMODE, int, string ) > MessageSignal;

///typedef boost::signals2::signal<void (int,string)> MessageSignal;
//typedef boost::function<void(int,const string)> MessageSignal;

/* YOU MUST NOT DISTURB THE ARRANGEMENT OF THE MEMORY SEGMENT OR STRUCTURE DEFINED TILL DO NOT DISTURB MARK.
 * USED FOR FETCHING RAW DATA FOR EXTENDED ANALYSIS IN STRATEGY*/
namespace UniqueDepthReader
{


    typedef struct
    {
        int Price;
        int Quantity;
    } DetailsT;

    using segment_manager_t = bip::managed_shared_memory::segment_manager;
    using void_allocator = boost::container::scoped_allocator_adaptor<bip::allocator<void, segment_manager_t>>;
    using int_allocator = void_allocator::rebind<DetailsT>::other;
    using int_vector = bip::vector<DetailsT, int_allocator> ;

    struct DepthDetailsT
    {
        int Token;
        int_vector Buy;
        int_vector Sell;
        DepthDetailsT ( const void_allocator &alloc ) : Buy ( alloc ), Sell ( alloc )
        {

        }
    };

    using SharedAllocatorT = bip::allocator<DepthDetailsT, bip::managed_shared_memory::segment_manager>;

    using DepthManagerT = bmi::multi_index_container<DepthDetailsT,
          bmi::indexed_by<
          bmi::ordered_non_unique<bmi::tag<struct Token>,
          BOOST_MULTI_INDEX_MEMBER ( DepthDetailsT, int, DepthDetailsT::Token ) >
          >, SharedAllocatorT>;

    using DepthViewT = nth_index<DepthManagerT, 0>::type;
}

typedef std::map<std::string, int> INCOLUMN;
typedef std::map<int, INCOLUMN> NETCOLUMN;

typedef vector<pair<OrderPacket, INORDERSTATUS>> _MultiOrderVector;
typedef map<double, _MultiOrderVector> OPKHolder;
typedef std::vector<OrderPacket> IOCOpkT;

typedef bip::allocator<
Order_Message, bip::managed_shared_memory::segment_manager
> shared_struct_allocator;


typedef bmi::multi_index_container<
Order_Message,
bmi::indexed_by<
bmi::ordered_unique
<bmi::tag<Order_Id>,  BOOST_MULTI_INDEX_MEMBER ( Order_Message, double, Order_Message::Order_Id ) >,
bmi::ordered_non_unique<bmi::tag<struct Composite>,
    bmi::composite_key<Order_Message,
    bmi::member<Order_Message, int, &Order_Message::Token>,
    bmi::member<Order_Message, char, &Order_Message::Order_Type>,
    bmi::member<Order_Message, int, &Order_Message::Price> > >
    >
    > Order_Set;


typedef nth_index<Order_Set, ORDERVIEW>::type Order_view;


/*
 *
 * DO NOT DISTRUB
 *
 *
 * */



typedef std::unordered_map<int, int> MessageCount;

typedef map<int, int> BAPrice;
typedef map<SIDE, BAPrice> PriceDepth;

/*	BANKNIFTY FUTURE DEPTH EXAMPLE
 *	SIDE	BAPrice
 *	_____	____________
 *	[BUY]	[25780][160]
 *		[25770][40]
 *	[SELL]	[25810][120]
 *		[25820][80]
 *
 * */

/*THIS CLASS HAS BEEN DESIGNED TO GET THE VALUE FROM ORDER POINTER.
 * USER IS SUPPOSED TO USE THIS CLASS TO REFRAIN HIMSELF FROM REMEMBERING
 * ALL THE ELEMENTS FOR THE ORDER STRUCTURE. A WILLING USER CAN DO THE SAME DIRECTLY
 * BY CALLING THE MEMBER ELEMENTS. THERE IS NO RESTRICTION ON THAT LEVEL.
 *
 * THE MOST IMPORTANT INPUT PARAM IS ORDER PACK */
class ORDERMANAGER
{
  public:

    ORDERMANAGER() =delete;

    /* RESTRICTLY TO BE USED BY INFRE ONLY*/
static bool UpdateInValues ( OrderPacket OPK, const INORDERSTATUS ORDSTAT, const double OrderNumber, const int Price );

    /* PARAMS
     * INORDERSTATUS : RETURNS THE ORDER STATUS RECIEVED FROM EXCHANGE
     * ExchangeOrderPackStruct : TO BE PASSED USING GET() METHOD OF SHARED_PTR ;
     *
     * IF OrderPack OPK IS MY ORDER POINTER THEN THE CODE TO CALL WOULD BE
     * EXAMPLE GetStatus(*OPK.get());
     *
     * */
    static const INORDERSTATUS GetStatus ( const ExchangeOrderPackStruct &OPK );

    /* PARAMS
     * INORDERSTATUS : RETURNS THE ORDER STATUS RECIEVED FROM EXCHANGE
     * ExchangeOrderPackStruct : TO BE PASSED USING GET() METHOD OF SHARED_PTR ;
     *
     * IF OrderPack OPK IS MY ORDER POINTER THEN THE CODE TO CALL WOULD BE
     * EXAMPLE GetOutStatus(*OPK.get());
     *
     * */

    static const OUTORDERSTATUS GetOutStatus ( const ExchangeOrderPackStruct &OPK );

    static const int GetLastPrice ( const ExchangeOrderPackStruct &OPK );

    static const double GetExchangeOrderNumber ( const ExchangeOrderPackStruct &OPK );

    static const int GetBLQ ( const ExchangeOrderPackStruct &OPK );

    static const int GetTickSize ( const ExchangeOrderPackStruct &OPK );

    static const int GetLastOrderQty ( const ExchangeOrderPackStruct &OPK ); //In Qty

    static const SIDE GetSide ( const ExchangeOrderPackStruct &OPK );
    static const int GetNetQty ( const ExchangeOrderPackStruct &OPK ); //In Lots
    static const int GetToken ( const ExchangeOrderPackStruct &OPK );
    static const int GetLastTradedQty ( const ExchangeOrderPackStruct &OPK ); //In Lots
    static const int GetLastTradedPrice ( const ExchangeOrderPackStruct &OPK );
    static const int GetPFNumber ( const ExchangeOrderPackStruct &OPK );
    static const int GetErrorCode ( const ExchangeOrderPackStruct &OPK );
    static const int PriceInTick ( const int Price, const int TickSize_, const SIDE _BS );
    static const int GetInfraGateway ( const ExchangeOrderPackStruct &OPK );
    static const int GetAvgPrice ( const ExchangeOrderPackStruct &OPK );
    static const int GetGateway ( const ExchangeOrderPackStruct &OPK );
    static const string GetOrderTag ( const ExchangeOrderPackStruct &OPK );
    /**static const int GetLowDPR (  OrderPacket OPK);
    static const int GetHighDPR (  OrderPacket OPK );
    static const int GetLowTER (  OrderPacket OPK );
    static const int GetHighTER (  OrderPacket OPK );
    static const void LoadLTP (OrderPacket  OPK , int*& LTP) ;
    static const int GetLTP ( OrderPacket OPK );**/
     static const void PreloadLowTER (OrderPacket  OPK, int*& TER);
    static const void PreloadLTP (OrderPacket  OPK, int*& LTP);
    static const void PreloadHighTER (OrderPacket  OPK, int*& TER);
    static const void PreloadLowDPR (OrderPacket  OPK, int*& DPR);
    static const void PreloadHighDPR (OrderPacket  OPK, int*& DPR);
    static const void PreloadSnapBid (OrderPacket  OPK , DepthPointT*& Depth);
    static const void PreloadSnapAsk (OrderPacket  OPK , DepthPointT*& Depth);



  protected:

  private:


};

class StrategyContext
{
  public:
    StrategyContext ( int PFNumber );
    virtual ~StrategyContext();
    void StopAll ( int PFNumber );
    void PublishPost ( boost::posix_time::ptime t1, PublishUpdate Value, const boost::system::error_code &ec );
    static void RegisterStrategy ( boost::shared_ptr<StrategyContext> Stg );
    void SetStrategyParams();
    void ReadyPlaceFast(OrderPacket OPK,int Price, int Qty);

    bool PlaceOrderPreLoad(int ServerCode, OrderPriority OP, IocOrderPacket VOPK,const int PRICEARRAY[]) ;
    bool ThreeLegPreloadGenerator(IocOrderPacket VOPK,int SRCP1, int PERP1, int SRCP2, int PERP2, int SRCP3, int PERP3,int QTY[]) ;

    /*void RegisterSymbol(OrderPacket OP,bool);

      void RegisterIocOrderToken(IocOpkT OPK);
      */

    bool PlaceOrder ( int ServerCode, OrderPriority OP, OrderPacket OPC );
    virtual void onParamsUpdate ( void *Params );

    bool PlaceOrder ( int ServerCode, OrderPriority OP, std::vector<OrderPacket> VOPK );
    //OrderPacket ReadyOrderPack(int Token, SIDE _BS,int ExcCode,ORDERTYPE LEGCOUNT,char Account[],char ALGOID[]/*MAX 20 CHAR*/,char ORDERTAG[]/*MAX 5 CHAR*/);
    OrderPacket ReadyOrderPack ( int Token, SIDE _BS, int ExcCode, ORDERTYPE LEGCOUNT, char Account[], char ALGOID[]/*MAX 20 CHAR*/, char ORDERTAG[]/*MAX 5 CHAR*/,bool IsIOC=false );
    OrderPacket ReadyOrderPack ( int Token, SIDE _BS, int ExcCode, ORDERTYPE LEGCOUNT, char Account[], char ALGOID[]/*MAX 20 CHAR*/, char ORDERTAG[]/*MAX 5 CHAR*/,int Depth ,bool IsIOC=false );

    IOCOpkT ReadyOrderPack ( std::vector<std::tuple<int, SIDE>> Details, int ExcCode, ORDERTYPE LEGCOUNT, char Account[], char ALGOID[]/*MAX 20 CHAR*/, char ORDERTAG[]/*MAX 5 CHAR*/ );
    IOCOpkT ReadyOrderPack(std::vector<std::tuple<int,SIDE>> Details,int ExcCode,ORDERTYPE LEGCOUNT,char Account[],char ALGOID[]/*MAX 20 CHAR*/,char ORDERTAG[]/*MAX 5 CHAR*/,int DepthSize);

    void RaiseSniffTrade ( const int TOKEN, const int PRICE, const int QTY, const int FILLNO, const short BS );

    virtual void OnSniffTrade ( const int TOKEN, const int PRICE, const int QTY, const int FILLNO, const short BS );
    void SetTimerCallback ( int Seconds );
    bool TokenRegistered;
    bool updateOutValues(IocOrderPacket OPK,const int Price[],const int Qty[],const OUTORDERSTATUS OUTSTAT);
    bool updateOutValues ( OrderPacket OPK, const int Price, const int Qty, const OUTORDERSTATUS OUTSTAT );
    void makeConnection ( boost::asio::io_service &io_service_ );
    void PostToStrategy ( OrderPacket OPK, INORDERSTATUS STATUS, ExchangeOrderPackStruct ByValueOPK, short ErrorCode_, short TCode, const boost::system::error_code &ec );


    void RegisterSymbol ( int SecurityID, int GateWayCode );
    bool RegisterOrderToken ( int SecurityID, int GateWayCode );

    MessageSignal RaiseMessage;
    MessageSignal RaiseImmediate;
    bool _bWorkerThread;
    void ActivateData();
    void DeactivateData();
    bool IsGatewayAvailable ( int GatewayCode );
    bool IsTokenAvailable ( int Token );
    virtual void TerminateStrategy();
    bool ResetToken ( int Token );
    //const UniqueDepthReader::DetailsT getTokenDetails ( OrderPacket OPK, int Ladder, SIDE side_ );
    const bool getTokenDetails(OrderPacket OPK, int Ladder, SIDE side_,DepthT*& OutDepth);

    NETCOLUMN GetNetBook ( int UID, std::string StartDate, std::string EndDate );

    double calculatePE ( const double &S, const double &K, const double &r, const double &v, const double &T );
    double calculateCE ( const double &S, const double &K, const double &r, const double &v, const double &T );

    double CalculateIV ( const double &S, const double &K, const double &r, const double &time, const double &option_price, bool IsCE = true );

    double CalculateDelta ( const double &S, const double &K, const double &r, const double &time, const double &vol, bool IsCE = true );
    void AppendLog ( std::string args );
  private:

  protected:
    int SendMessage ( int PFNumber, const string &Msg );
    int SendImmediate ( int PFNumber, const string &Msg );

    void WorkerThread();
    void OnOrderSequencer ( boost::posix_time::ptime t1, PublishUpdate ORDER );
    bool UpdateInValues ( OrderPacket OPK, const INORDERSTATUS ORDSTAT, const double OrderNumber, const int Price );
    void RegisterOrderToken ( OrderPacket OP );
    virtual void OnMarketDataEvent ( boost::posix_time::ptime t1, PublishUpdate Token ) =0;
    void OnMarketSequencer ( boost::posix_time::ptime t1, PublishUpdate Token );
    virtual void OnOrderUpdateEvent ( boost::posix_time::ptime t1, PublishUpdate ORDER ) =0;
    virtual void OnTimerUpdate() =0; //ExchangeOrderPackStruct * OP);
    void PFDisposalCallBack ( int Seconds, int _PFNumber );
    void OrderTimer ( int _PFNumber, bool IsDeadLineStarted );
    boost::shared_ptr< boost::asio::io_service::strand > strand;
    boost::shared_ptr <boost::asio::deadline_timer> deadline_;
    vector<OrderPacket> _OrderHolder;


    boost::shared_ptr<bip::managed_shared_memory> Segment;
    std::pair<UniqueDepthReader::DepthManagerT *, size_t> DepthManager;

    UniqueDepthReader::DepthManagerT::iterator depthIterator_;

    int PFNumber;
    int countX;
    pthread_mutex_t OrderMutex;
    bool DataConsumed;
    int SingleToken;
    MessageCount _MessageCount;
    OPKHolder _OPKHolder;
};

#endif // SHAREDCLASSES_H_INCLUDED
