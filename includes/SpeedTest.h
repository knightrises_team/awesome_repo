
#include "../includes/SharedClasses.h"
#include "../includes/Logger.h"

class SpeedTest : public StrategyContext , public Logger
{
	public :
		SpeedTest(int PFNumber,void * Params);
		static void getInst(int PF,void * Params);
		virtual void OnMarketDataEvent(boost::posix_time::ptime t1,PublishUpdate Token);
		virtual void OnOrderUpdateEvent(boost::posix_time::ptime t1,PublishUpdate Update);
		void TerminateStrategy();
		virtual ~ SpeedTest();
        void GetReady() ;
		virtual void onParamsUpdate(void * Params);
		virtual void OnSniffTrade(const int TOKEN, const int PRICE, const int QTY,const int FILLNO, const short BS);
		bool ReplaceCancel(OrderPacket OPK,int Quantity, int UpdateTicks,int Price);
	        bool PlaceNew(OrderPacket OPK,int Quantity, int UpdateTicks,int Price);


	protected:
		OrderPacket OrderLeg;
		Logger *Log = new Logger();
		OrderPacket RevOrderLeg;
		bool PlaceFinalOrder(OrderPacket OPK,int Qty,int UPDATETICK,int FinalPrice);
		virtual void OnTimerUpdate();//ExchangeOrderPackStruct * OP);
		void OnFeed();
		bool IsTerminateOn;
		int PFNumber,U1DPR,L1DPR;
		int UPDATETICK;
		int PLACEFLAG = 0 ;
		int SP = 0 ;
		int RELEASE = 0 ;
		bool FE = false ;
		bool ParamsCalled =  false ;
		int Depth = 5 ;
		int STP = 0 ;
		DepthT *Leg1Bid[5] = {NULL} ;
        DepthT *Leg1Ask[5] = {NULL} ;
        DepthT *Leg2Bid[5] = {NULL};
        DepthT *Leg2Ask[5] = {NULL};
        int elapsedtime= 0 ;
        int timeframe = 6 ;
        bool letsgo = false  ;
        int counter = 0 ;
		int BLQ = 0 ;
		int Token;
		int NFinalPrice , NLASTPRICE ;
		int OrderLots;
	 string T1CC ;
		int TotalLots , Net{0};
		int FinalPrice;
		SIDE BS;
		int GATEWAYCODE;
		int BIDPRICE;
		int BIDPRICE2 = 0 ;
		int ASKPRICE;
		int ASKPRICE2 = 0 ;
		bool IsDepth;
		int LASTPRICE,LastStatus;
		bool IsTimerRunning = false ;
		int TokenTick;
		int Time{0};
};
