#ifndef SHAREDHEADER_H_INCLUDED
#define SHAREDHEADER_H_INCLUDED

#include <boost/atomic.hpp>
#include <boost/shared_ptr.hpp>

#include <string.h>
#include <mutex>
#include <vector>

using namespace boost;
/*
 * */
extern bool _bDepthDataRequired;
enum LOGMODE
{
    PFMSG=0,
    UPDMSG,
    TTMSG,
    LOGMSG
};

enum inDataRequestType
{
    PRICEUPDATE=0,
    ORDERUPDATE,
    DATAERROR
};

struct PublishUpdate
{
    inDataRequestType InRequestDetail;
    int Token;
    double OrderNumber;
    short ErrorCode;
    short TCode; // Temporary
};

enum OrderPriority
{
    Normal=1,
    High
};

enum SIDE
{
    BUY=1,
    SELL
};

enum INORDERSTATUS
{
    NONE=0,
    NEW,
    REPLACED,
    CANCELLED,
    PARTIALFILLED,
    FILLED,
    NEWREJECTED,
    MODREJECTED,
    CANREJECTED,
    PENDING,
    REJECTED,
    INFRAREJECTED
};

enum OUTORDERSTATUS
{
    OUTNONE=0,
    PLACE,
    REPLACE,
    CANCEL,
    PLACESPD,
    REPLACESPD,
    CANCELSPD,
    PLACEIOC,
    REPLACEIOC,
    CANCELIOC,
    CANCELALL,
    REPLACEFAST,
    PLACEFAST,
    HEARTBEAT
};


enum ORDERTYPE
{
    SINGLELEG=1,
    TWOLEGIOC,
    THREELEGIOC
};

 #pragma pack(push, 1)

typedef struct
{
    int Price;
    int Quantity;
    int Orders;
} DepthPointT;

typedef struct
{
    DepthPointT Bid[5];
    DepthPointT Ask[5];

    uint64_t Time;
    int LTP;
    int LTQ;
    char LTTime[30];

    int ATP;
    float PercentChange;

    int Open;
    int High;
    int Low;
    int Close;

    int LowExecBand;
    int HighExecBand;

    int HighDPR;
    int LowDPR;

    uint64_t TotalBuyQuantity;
    uint64_t TotalSellQuantity;
    uint64_t VolumeTradedToday;

    int OpenInterest;

} SnapshotDetailsT;


#pragma pack(pop)

#pragma pack(push,1)

#define DEPTHSIZE 5
typedef struct
{
    int Price = 0;
    int Quantity = 0;

}DepthT;

struct TBTDepthT
{
    uint32_t Token;
    DepthT Buy[DEPTHSIZE];
    DepthT Sell[DEPTHSIZE];

    int LTP;
    int LTQ;
    int LTT;
    short Exch;

};

struct OrderElements
{
    int Token;
    SIDE _BS;
    int Price;
    int Qty;
    char CLICode[10];
    char ALGOID[20];
    char PANCARD[10];
    int ExcCode;
    int ERRORCODE;
    short LC;
    double ExchangeOrderNumber;
    INORDERSTATUS Status;

public:
    OrderElements():Status(NONE),Token(0),_BS(BUY),Price(0),Qty(0),ExcCode(0),LC(0),ExchangeOrderNumber(0),ERRORCODE(0)
    {
        memset(CLICode,'\0',10);
        memset(ALGOID,'\0',20);
        memset(PANCARD,'\0',10);
    }
};

struct TokenPosition
{
    int LastQty;
    int LastTradedPrice;
    int NetQty;
    int AvgPrice;
};

struct TokenDetails
{
    int TickSize;
    int BLQ;

//public:
    //  TokenDetails(int Ticks, int _BLQ):TickSize(Ticks),BLQ(_BLQ){}
};

struct ExchangeOrderPackStruct
{

public:
    ExchangeOrderPackStruct()
    {
        memset(&ElemPrime,0,sizeof(OrderElements));
        memset(&Position,0,sizeof(TokenPosition));
        memset(&Details,0,sizeof(TokenDetails));
        memset(ORDERTAG,'\0',5);

        this->OUTSTAT_=OUTNONE;
        this->UID=-1;
        this->SocketClass = NULL;
        this->ExchangeClass = NULL;
        this->InfraGateway = 0;
        this->IsIOC = false;
        this->TBT = NULL;
        this->LASTIN = NONE;
        this->Pointer_To_NSE_Orders=NULL;
    }
    ~ExchangeOrderPackStruct()
    {


    }

    OrderElements ElemPrime;

    TokenPosition Position;

    TokenDetails Details;

    uint64_t UID;
    OUTORDERSTATUS OUTSTAT_;
    int InfraGateway;
    char ORDERTAG[5];
    bool IsIOC;
    ///char Reserved[50]; ///EARLIER 55 NOW (55 - Multiplier(4 bytes) -bool)
    OrderPriority OPRT;///4 bytes deducted from Reserved


    /** Before Adding TransportPaket New
    ///char Reserved[38]; ///EARLIER 55 NOW (55 - Multiplier(4 bytes) -bool) - void * (8byte) -4 byte (OrderPriority)
    **/
    /** new size after TransportPacket**/
    ///char Reserved[30];


    /** new size after LASTSTATUS 20190911**/
    ///char Reserved[26];

    /** new size after LASTSTATUS 20191114**/
    char Reserved[18];

   /// void* SHMPointer;

   SnapshotDetailsT * SHMPointer;

    INORDERSTATUS LASTIN; /// 4 byte for LastStatus

    void * Pointer_To_NSE_Orders; /// 8 byte for pointer

    TBTDepthT * TBT;
    int Multiplier;
    void * ExchangeClass;
    void * SocketClass;
};


#pragma pack(pop)


#pragma pack(push,2)
struct OrderHashElement
{
    const int Token;
    const short _BS;
    const int Price;
    const int Qty;

    OrderHashElement ( const int _Token, const short BS, const int _Price, const int _Qty ) :Token ( _Token ), _BS ( BS ), Price ( _Price ), Qty ( _Qty )
    {}
};
#pragma pack(pop)


typedef boost::shared_ptr<ExchangeOrderPackStruct> OrderPacket;
typedef std::vector<OrderPacket> IocOrderPacket;

struct DataPoint
{
  public:
    DataPoint ( int Token_,    int PricePoint_,    int Qty_ ) :Token ( Token_ ), PricePoint ( PricePoint_ ), Qty ( Qty_ ), InstanceCount ( 1 )
    {
    }
    int Token;
    int PricePoint;
    int Qty;
    int InstanceCount;
};

# pragma pack(push, 1)
struct Order_Message ///For Data
{
    //STREAM_HEADER Global_Header;
    //char Message_Type;
    double Timestamp;
    double Order_Id;
    int Token;
    char Order_Type;
    int Price;
    int Quantity;

    inline bool operator > ( const Order_Message &rhs ) const
    {
        return ( Price > rhs.Price );
    }

};

struct Trade_Message ///FOr Data
{
    //STREAM_HEADER Global_Header;
    //char Message_Type;
    double Timestamp;
    double Buyorder_Id;
    double Selloder_Id;
    int Token;
    int Trade_Price;
    int Trade_Quantity;
};



#pragma pack(pop)

struct Order_Id {};
//struct Token{};
struct Order_Type {};
struct Price {};
struct OrderNo {};
struct DataPack {};


struct MIOrder_Message ///For Data
{
    //STREAM_HEADER Global_Header;
    //char Message_Type;

    MIOrder_Message ( Order_Message _Ord )
        :Timestamp ( _Ord.Timestamp ), Order_Id ( _Ord.Order_Id ),
         Token ( _Ord.Token ), Order_Type ( _Ord.Order_Type ),
         Price ( _Ord.Price ), Quantity ( _Ord.Quantity )
    {
        //  std::cout << " Insert data for Token "<< _Ord.Token<<std::endl;
    }

    double Timestamp;
    double Order_Id;
    int Token;
    char Order_Type;
    int Price;
    int Quantity;
};

struct change_OrderDetails
{
    change_OrderDetails ( Order_Message _Ord ) :_omsg ( _Ord ) {}

    void operator() ( MIOrder_Message &e )
    {
        e.Price= _omsg.Price;
        e.Quantity = _omsg.Quantity;
        e.Timestamp = _omsg.Timestamp;

        //std::cout << " Updated data for Token "<< _omsg.Token<<std::endl;
    }

  private:
    Order_Message _omsg;
};
enum InstType
{
    FUTCUR = 0,
    FUTIDX,
    FUTIRC,
    FUTIRT,
    FUTIVX,
    FUTSTK,
    OPTCUR,
    OPTIDX,
    OPTSTK,
    DEFAULT = -1
};



#endif // SHAREDHEADER_H_INCLUDED
