#!/bin/bash

rm -rf build/
mkdir build
cd build
rm -rf ../bin/
mkdir -p ../bin/Release
cmake .. > ../bin/Release/CmakeLog.txt 2>&1
make > ../bin/Release/makeLog.txt 2>&1

