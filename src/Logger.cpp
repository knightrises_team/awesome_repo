#include "../includes/Logger.h"

#include <string.h>
#include <boost/iostreams/flush.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <sys/time.h>
#define FORMAT "%Y-%m-%d %H:%M:%S"
//#define FORMAT "%a %Y/%m/%d %H:%M:%"
#define BUF_SIZE 80
#define MAXBUFF 20

//

using namespace std ;

Logger::Logger() : active(false)
{
    cout << "Creating new Logger instance for  "<< endl ;
}


void Logger::Start(bool ACTIVE , Priority minPriority, const string& logFile)
{
    cout << "Logger instance activated " << endl ;
    active = ACTIVE ;
    minPriority = minPriority;
    cout << "Logger priority set LOGFILE : " << logFile << endl ;

    if ( active && logFile != "" )
    {
        cout << "Opening file to stream file ..." << endl ;
        fileStream.open(logFile.c_str(), fstream::out | fstream::app);

        //stream = fileStream.is_open() ? fileStream : std::cout;
    }
}

void Logger::Stop()
{

    if (fileStream.is_open())
    {
        active = false;
        //cout << "Stopping Logger" << endl ;
        fileStream.close();
    }
}

void Logger::Flush()
{

    if ( active && fileStream.is_open())
    {
        //cout << "Flushing Logger" << endl ;
        fileStream.flush();
    }
}

/**
std::string Logger::LocalTime()
{
    struct timeval tv;
    struct tm* ptm;
    char time_string[50];
    char ms[10];
    long milliseconds;


    gettimeofday (&tv, NULL);
    ptm = localtime (&tv.tv_sec);

    strftime (time_string, sizeof (time_string), "%H:%M:%S", ptm);

    //milliseconds = tv.tv_usec / 1000;
    //sprintf(ms,".%0 "%Y-%m-%d %H:%M:%S"3ld",milliseconds);
    //strcat(time_string,ms);
    return time_string;
}

//std::string Logger::timeStampToHReadbletillns(long rawtime)
void Logger::timeStampToHReadbletillns(ostream &os)
{


    //long rawtime = __builtin_bswap64(rawtime_);


    //return string(buf);
    /**

        char buffer[32];
        std::time_t now = time(0);
        std::tm *time = localtime(&now);
        strftime(buffer, sizeof(buffer), "%a %Y/%m/%d %H:%M:%S" , time);
        return string(buffer);




}
//*/
void Logger::Write(Priority priority, int line, const string& message)
{

    try
    {

        //time1.start() ;

        if (active && priority >= minPriority)
        {
            //int k = 0 ;
            //cout << "conditions satisfied starting to write in file " << endl

            //boost::posix_time::ptime ptime = boost::posix_time::microsec_clock::local_time() ;

            //boost::posix_time::ptime now(boost::posix_time::microsec_clock::local_time());

            //time2.start();
            ostream &stream = fileStream.is_open() ? fileStream : std::cout;

            //stream.rdbuf()->pubsetbuf(hevc, 100);

            gettimeofday(&(currentTime), NULL);

            rawtime = currentTime.tv_sec * (int)1e9 + currentTime.tv_usec ;

            timeinsec = rawtime/1000000000;

            timeinns = rawtime%1000000000;

            dt = localtime (&timeinsec);

            strftime(buf, BUF_SIZE, FORMAT, dt);

            sprintf(buf,"%s.%ld",buf,timeinns);
              // << LOGGER_TIME_IN_HUMAN_READABLE(now)


            (line) ? stream << counter << " : "
                    << "[" << PRIORITY_NAMES[priority] << "] "
                   << "[" <<buf << "] : "
                   << "[" << line << "] "
                   << message
                   << "\n" :
                   stream << counter << " : "
                    << "[" << PRIORITY_NAMES[priority] << "] "
                   << "[" <<buf << "] : "
                   << message
                   << "\n";


            //k = time2.get_elapsed_ns() ;
            //cout << "Time to write LOG : " << k << "  " << __PRETTY_FUNCTION__ << endl ;

            //int t = time1.get_elapsed_ns() ;

            //cout << "Time to print : " << t << "  " << LOGFILE << endl ;

            if(buffcounter >= MAXBUFF)
            {
                buffcounter = 0 ;
                LOGGER_FLUSH() ;
            }

            counter ++ ;
            buffcounter ++ ;
        }
    }
    catch  (const string& str)
    {
        cout << Logger::ERROR << __LINE__ << "  exception caught: " << str <<endl ;
        LOGGER_FLUSH() ;
        exit(EXIT_FAILURE);
    }

}




