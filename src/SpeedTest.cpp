#include <stdlib.h>
#include "../includes/CONTRACTINFO.h"
#include "../includes/SpeedTest.h"
#include "../includes/plf_nanotimer.h"

plf::nanotimer timer;

#define NSETICKS 5

SpeedTest::SpeedTest(int PFNumber,void * Params):
    StrategyContext(PFNumber),PFNumber(PFNumber),UPDATETICK(0)
{
    Token=0;
    OrderLots= L1DPR = U1DPR = 0;
    TotalLots=0;
    FinalPrice=0;
    IsTerminateOn = false;
    LASTPRICE =0;
    cout << "CREATING LOGFILE" << "  " << __PRETTY_FUNCTION__ << endl ;
    const string LOGFILE = string("logs/") + std::to_string(PFNumber) + string(".log") ;


    Log -> LOGGER_START(true ,Logger::CONSTRUCT, LOGFILE) ;

    Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__, "Input file is " + LOGFILE ) ;

    Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__,"Firing OnParams Update");

    onParamsUpdate(Params);

    if(Token !=0)
    {

        Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__,"Calling ReadyOrderPack ");

        if(RELEASE == 11)
        {

            //AppendLog("ReadyOrderPack with Release 11 !!!");
            Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__,"ReadyOrderPack with Release 11 !!!");
            OrderLeg = StrategyContext::ReadyOrderPack(Token,BS,GATEWAYCODE,SINGLELEG,T1CC.c_str(),"","",false  );
        }
        else
        {

            //AppendLog("ReadyOrderPack with Release 12 !!!");
            Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__,"ReadyOrderPack with Release 12 !!!");
            OrderLeg = StrategyContext::ReadyOrderPack(Token,BS,GATEWAYCODE,SINGLELEG,T1CC.c_str(),"","",Depth,false  );
            RevOrderLeg = StrategyContext::ReadyOrderPack(Token,((BS ==  BUY) ? SELL : BUY ),GATEWAYCODE,SINGLELEG,T1CC.c_str(),"","",Depth,false );
        }

        Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__,"ReadyOrderPack Done");

        RevOrderLeg->UID = PFNumber;
        OrderLeg->UID = PFNumber;

        Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__,"PF Set");
        /**
        L1DPR = CONTRACTINFO::GetLowerDPRForSecurity(Token);
        	 U1DPR = CONTRACTINFO::GetUpperDPRForSecurity(Token);

        ReadyPlaceFast(OrderLeg ,(BS == BUY) ? U1DPR : L1DPR, ORDERMANAGER::GetBLQ(*OrderLeg));**/

        TokenTick= ORDERMANAGER::GetTickSize(*OrderLeg.get());
    }

    BLQ = ORDERMANAGER::GetBLQ(*OrderLeg);

    Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__,"Token\t" +std::to_string(Token) + "\tTICKSIZE\t" + std::to_string(TokenTick) + "\tFINALTICK\t" + std::to_string(UPDATETICK) );

    /* if(CONTRACTINFO::IsOption(Token))
         AppendLog("OPTION found");
     if(CONTRACTINFO::IsFuture(Token))
         AppendLog("FUTURE found");

     StrategyContext::AppendLog("Initiated ParamsCalled : "+ std::to_string(ParamsCalled));
    **/

}

SpeedTest::~SpeedTest()
{

}


bool SpeedTest::PlaceFinalOrder(OrderPacket OPK,int Quantity, int UpdateTicks,int Price)
{
    Log ->  LOGGER_WRITE(Logger::PLCFO, __LINE__, "PFO for TKN : "+ to_string ( ORDERMANAGER::GetToken ( *OPK ) ) + "    QTY : " + to_string ( Quantity ) + "    FP : " + to_string ( FinalPrice ) + "    STAT : " + std::to_string ( ORDERMANAGER::GetStatus ( *OPK ) ) + "  PF : " + std::to_string ( PFNumber ) );
    bool IsOrderPlaced = false;
    switch(ORDERMANAGER::GetStatus(*OPK))
    {
    case INFRAREJECTED:
    {
        if(ORDERMANAGER::GetOutStatus(*OPK) == OUTNONE || ORDERMANAGER::GetOutStatus(*OPK) == PLACE || ORDERMANAGER::GetOutStatus(*OPK) == PLACEFAST )
        {
            IsOrderPlaced = PlaceNew(OPK, Quantity,UpdateTicks,Price);
        }
        else if(ORDERMANAGER::GetOutStatus(*OPK) == REPLACE || ORDERMANAGER::GetOutStatus(*OPK) == REPLACEFAST or ORDERMANAGER::GetOutStatus(*OPK) == CANCEL )
        {
            IsOrderPlaced = ReplaceCancel( OPK, Quantity,UpdateTicks,Price);
        }
        break;
    }
    case MODREJECTED:
    case CANREJECTED:
    {
        if(ORDERMANAGER::GetStatus(*OPK) == MODREJECTED)
        {
            Log ->  LOGGER_WRITE(Logger::PLCFO, __LINE__,"STATUS IS : MODREJECTED !!!");
        }
        else
            Log ->  LOGGER_WRITE(Logger::PLCFO, __LINE__,"STATUS IS : CANCELREJECTED !!!");

        if(LastStatus == FILLED)
        {
            Log ->  LOGGER_WRITE(Logger::PLCFO, __LINE__,"Filled After Modrejected");
            IsOrderPlaced = PlaceNew( OPK, Quantity,UpdateTicks,Price);
            break;
        }


    }
    case NEW:
    case REPLACED:
    case PARTIALFILLED:
    {
        IsOrderPlaced = ReplaceCancel( OPK, Quantity,UpdateTicks,Price);
        break;
    }
    case PENDING:
    {
        break;
    }
    case NEWREJECTED:
    {
        Log ->  LOGGER_WRITE(Logger::PLCFO, __LINE__,"Status is NEWREJECTED");
        TerminateStrategy();
        break;
    }
    default:
    {
        IsOrderPlaced = PlaceNew( OPK, Quantity,UpdateTicks,Price);
        break;
    }


    }
    return IsOrderPlaced;
}


bool SpeedTest::ReplaceCancel ( OrderPacket OPK, int Quantity, int UpdateTicks, int Price )
{
    bool IsOrderPlaced = false;

    if ( UpdateTicks == 2 && updateOutValues ( OPK, Price, Quantity*BLQ, ((PLACEFLAG == 0) ? REPLACE : REPLACEFAST) ) && PlaceOrder ( OPK->ElemPrime.ExcCode, High, OPK ))
    {
        //  AppendLog ( "Replaced Hedge Order with ReplaceFast and High Priorty" );
        return true;
        //AppendLog ( "Replaced Order with High Priorty" );RevOrderLeg = StrategyContext::ReadyOrderPack(Token,BS,GATEWAYCODE,SINGLELEG,T1CC.c_str(),"","",Depth,false  );
    }
    else if ( UpdateTicks == 1 && updateOutValues ( OPK, Price, Quantity*BLQ, ((PLACEFLAG == 0) ? REPLACE : REPLACEFAST) ) && PlaceOrder ( OPK->ElemPrime.ExcCode, Normal, OPK ))
    {
        // AppendLog ( "Replaced Bid Order with ReplaceFast and Normal Priorty" );
        return true;RevOrderLeg = StrategyContext::ReadyOrderPack(Token,BS,GATEWAYCODE,SINGLELEG,T1CC.c_str(),"","",Depth,false  );
        // AppendLog ( "Replaced Order with Normal Priorty" );
    }
    else if ( UpdateTicks == 0 && updateOutValues ( OPK, Price, Quantity*BLQ, ((PLACEFLAG == 0) ? REPLACE : REPLACEFAST) ) && PlaceOrder ( OPK->ElemPrime.ExcCode, Normal, OPK ))
    {
        Log ->  LOGGER_WRITE(Logger::PLCFO, __LINE__,"REP for TKN : "+ to_string ( ORDERMANAGER::GetToken ( *OPK ) ) + "    QTY : " + to_string ( Quantity ) + "    FP : " + to_string ( FinalPrice ) + "    STAT : " + std::to_string ( ORDERMANAGER::GetStatus ( *OPK ) ) + "  PF : " + std::to_string ( PFNumber ) );
        return true;
        // AppendLog ( "Replaced Order with Normal Priorty" );
    }
    else if ( UpdateTicks == -1  && updateOutValues ( OPK, Price, Quantity*BLQ, CANCEL ) && PlaceOrder ( OPK->ElemPrime.ExcCode, Normal, OPK ))
    {
        // AppendLog ( "Cancelled Bid Order with Normal Priorty" );
        return true;
        // AppendLog ( "Canceling Order with Normal Priorty" );
    }
    else if ( UpdateTicks == -2  && updateOutValues ( OPK, Price, Quantity*BLQ, CANCEL ) && PlaceOrder ( OPK->ElemPrime.ExcCode, High, OPK ))
    {RevOrderLeg = StrategyContext::ReadyOrderPack(Token,BS,GATEWAYCODE,SINGLELEG,T1CC.c_str(),"","",Depth,false  );
        // AppendLog ( "Cancelled Bid Order with High Priorty" );
        return true;
        // AppendLog ( "Canceling Order with High Priorty" );
    }
    return IsOrderPlaced;
}


bool SpeedTest::PlaceNew ( OrderPacket OPK, int Quantity, int UpdateTicks, int Price )
{
    bool IsOrderPlaced = false;

    if ( UpdateTicks == 2 && updateOutValues ( OPK, Price, Quantity*BLQ, ((PLACEFLAG == 0) ? PLACE : PLACEFAST) ) && PlaceOrder ( OPK->ElemPrime.ExcCode, High, OPK ))
    {
        // AppendLog ( "Placed Hedge New Order with PlaceFast and High Priorty" );
        return true;

    }
    else if ( (UpdateTicks == 1 || UpdateTicks == 0 )&& updateOutValues ( OPK, Price, Quantity*BLQ, ((PLACEFLAG == 0) ? PLACE : PLACEFAST) ) && PlaceOrder ( OPK->ElemPrime.ExcCode, Normal, OPK ))
    {
        Log ->  LOGGER_WRITE(Logger::PLCFO, __LINE__,"NEW for TKN : "+ to_string ( ORDERMANAGER::GetToken ( *OPK ) ) + "    QTY : " + to_string ( Quantity ) + "    FP : " + to_string ( FinalPrice ) + "    STAT : " + std::to_string ( ORDERMANAGER::GetStatus ( *OPK ) ) + "  PF : " + std::to_string ( PFNumber ) );
        return true;
        // AppendLog ( "Placing New Order with Normal Priorty" );
    }


    return IsOrderPlaced;
}

void SpeedTest::TerminateStrategy()
{
    IsTerminateOn = true ;
    Log ->  LOGGER_WRITE(Logger::INFO, __LINE__, " Stop Request intiated from Strategy PF " + std::to_string( PFNumber) );
    StopAll(PFNumber);
}
void SpeedTest::OnMarketDataEvent(boost::posix_time::ptime t1,PublishUpdate DataUpdate)
{

    Log -> LOGGER_WRITE(Logger::MKT , __LINE__,"---------------------- Market Event Invoked ---------------------");

    using namespace boost::posix_time;
    boost::posix_time::ptime const time_epoch(boost::gregorian::date(1970, 1, 1));


    auto x = (t1 - time_epoch).total_nanoseconds();

    auto cur_ms = (boost::posix_time::microsec_clock::local_time() - time_epoch).total_nanoseconds();
    //boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1)) ;


    auto z = cur_ms -x;
    //long val =  (diff.ticks() / diff.ticks_per_second());

    //std::cout << "t1 : " << x << "  cur_ms : " << cur_ms << "  diff :  "<< z <<  "\n";

    Log -> LOGGER_WRITE(Logger::MKT, __LINE__,"MKT_TIME : "+ std::to_string(x)+"  CUR_TIME : "+ std::to_string(cur_ms)+"  DIFF : "+ std::to_string(z));

    if(letsgo)
    {

        for(int i = 1 ; i <=Depth ; i++)
        {
            Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__,"BID  ["+std::to_string(i)+"]  : "+std::to_string( Leg1Bid[i-1] -> Price ) + "     |     ASK  ["+std::to_string(i)+"]  : "+std::to_string(Leg1Ask[i-1] -> Price) ) ;

            if( Leg1Bid[i-1] -> Price > Leg1Ask[i-1] -> Price)
            {
                Log -> LOGGER_WRITE(Logger::ERROR , __LINE__,"WRONG : bid is greater than ask ");
            }
        }

        BIDPRICE = Leg1Bid[0] -> Price ;
        ASKPRICE = Leg1Ask[0] -> Price ;
        BIDPRICE2 = Leg2Bid[0] -> Price ;
        ASKPRICE2 = Leg2Ask[0] -> Price ;

        if((ORDERMANAGER::GetNetQty(*OrderLeg)/ORDERMANAGER::GetBLQ(*OrderLeg)) != Net)
            Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__,"NetQty ("+std::to_string(ORDERMANAGER::GetNetQty(*OrderLeg)/ORDERMANAGER::GetBLQ(*OrderLeg))+")  != Saved Net ("+std::to_string(Net)+")");

        if(BIDPRICE && ASKPRICE)
        {
            FinalPrice = BS == BUY ? ( BIDPRICE - (UPDATETICK * ORDERMANAGER::GetTickSize(*OrderLeg))  ) : ( ASKPRICE + (UPDATETICK *  ORDERMANAGER::GetTickSize(*OrderLeg)) );

            if(ORDERMANAGER::GetNetQty(*OrderLeg)/ORDERMANAGER::GetBLQ(*OrderLeg) < TotalLots && FinalPrice >= NSETICKS && FinalPrice!= LASTPRICE)
            {
                int LegOrderQty =std::min(TotalLots - ORDERMANAGER::GetNetQty(*OrderLeg)/ORDERMANAGER::GetBLQ(*OrderLeg),OrderLots ) ;
                //if((BS == BUY) ? (BIDPRICE != LASTPRICE) : (ASKPRICE != LASTPRICE) )
                {
                    if(PlaceFinalOrder(OrderLeg,LegOrderQty,1,FinalPrice))
                        LASTPRICE = FinalPrice;
                    else
                        Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__,"OrderNotPlaced");
                }
            }
        }

    }

    if(!IsTimerRunning)
    {
        Log -> LOGGER_WRITE(Logger::CONSTRUCT , __LINE__,"Starting timer ...") ;
        StrategyContext::SetTimerCallback(Time);
        IsTimerRunning = true;

    }


}

void SpeedTest::GetReady()
{

    Log -> LOGGER_WRITE(Logger::TIMER , __LINE__,"Going to calculate prices via gtd");

    for ( int i = 0 ; i <= Depth ; i ++ )
    {
        if(Leg1Bid[i] == NULL)
        {
            if ( getTokenDetails( OrderLeg, i+1, BUY, Leg1Bid[i] ) )
            {
                counter ++ ;
                Log -> LOGGER_WRITE(Logger::TIMER , __LINE__,"GetTokendeatils Leg1Bid [" + std::to_string(i) + "] return true");
            }
            else
                Log -> LOGGER_WRITE(Logger::TIMER , __LINE__,"GetTokendeatils Leg1Bid [" + std::to_string(i) + "] return false");
        }

        if(Leg1Ask[i] == NULL)
        {
            if(getTokenDetails( OrderLeg, i+1, SELL, Leg1Ask[i] ))
            {
                counter ++ ;
                Log -> LOGGER_WRITE(Logger::TIMER , __LINE__,"GetTokendeatils Leg1Ask [" + std::to_string(i) + "] return true");
            }
            else
                Log -> LOGGER_WRITE(Logger::TIMER , __LINE__,"GetTokendeatils Leg1Ask [" + std::to_string(i) + "] return false");
        }

        if(Leg2Bid[i] == NULL)
        {
            if ( getTokenDetails( RevOrderLeg, i+1, BUY, Leg2Bid[i] ) )
            {
                counter ++ ;
                Log -> LOGGER_WRITE(Logger::TIMER , __LINE__,"GetTokendeatils Leg2Bid [" + std::to_string(i) + "] return true");
            }
            else
                Log -> LOGGER_WRITE(Logger::TIMER , __LINE__,"GetTokendeatils Leg2Bid [" + std::to_string(i) + "] return false");
        }

        if(Leg2Ask[i] == NULL)
        {
            if(getTokenDetails( RevOrderLeg, i+1, SELL, Leg2Ask[i] ))
            {
                counter ++ ;
                Log -> LOGGER_WRITE(Logger::TIMER , __LINE__,"GetTokendeatils Leg2Ask [" + std::to_string(i) + "] return true");
            }
            else
                Log -> LOGGER_WRITE(Logger::TIMER , __LINE__,"GetTokendeatils Leg2Ask [" + std::to_string(i) + "] return false");
        }
    }

    if(counter >= ( 4 * Depth ))
    {
        Log -> LOGGER_WRITE(Logger::TIMER , __LINE__,"Setting letsgo to true");
        letsgo = true ;
    }

    if(letsgo)
    {
        Log -> LOGGER_WRITE(Logger::TIMER , __LINE__,"Calculating BID ASK ...") ;
        BIDPRICE = Leg1Bid[0] -> Price ;
        ASKPRICE = Leg1Ask[0] -> Price ;
        BIDPRICE2 = Leg2Bid[0] -> Price ;
        ASKPRICE2 = Leg2Ask[0] -> Price ;
    }


}


void SpeedTest::OnTimerUpdate()
{
    if(!letsgo)
    {
        GetReady();
    }

    /**AppendLog("time frame : " +std::to_string(timeframe) + "  elapsed time : " + std::to_string(elapsedtime));

    if(elapsedtime >= timeframe)
    {
        AppendLog("ReadyOrderPack Leg 2 in timer for time frame : " + std::to_string(timeframe));
        RevOrderLeg = StrategyContext::ReadyOrderPack(Token,((BS ==  BUY) ? SELL : BUY ),GATEWAYCODE,SINGLELEG,T1CC.c_str(),"","",Depth,false );
        RevOrderLeg -> UID = PFNumber ;

        AppendLog("ReadyOrderPack Leg 1 in timer for time frame : " + std::to_string(timeframe));
        OrderLeg = StrategyContext::ReadyOrderPack(Token,BS,GATEWAYCODE,SINGLELEG,T1CC.c_str(),"","",Depth,false );
        OrderLeg -> UID = PFNumber ;
        elapsedtime = 0 ;
    }

    ++elapsedtime  ;**/

    RaiseMessage(UPDMSG,ORDERMANAGER::GetPFNumber(*OrderLeg.get()),"FinalPrice=" + std::to_string(FinalPrice) + ",PRICETICK="+std::to_string(UPDATETICK * TokenTick ) );
    StrategyContext::SetTimerCallback(Time);


}

void SpeedTest::OnOrderUpdateEvent(boost::posix_time::ptime t1,PublishUpdate Update)
{
    if(IsTerminateOn)
        return ;
//	AppendLog("PublishUpdate OrderNumber : " + std::to_string(Update.OrderNumber));
//	AppendLog("OrderPKT OrderNumber : " + std::to_string(ORDERMANAGER::GetExchangeOrderNumber(*OrderLeg.get())));


    if(Update.OrderNumber == ORDERMANAGER::GetExchangeOrderNumber(*OrderLeg.get()))
    {
        Log -> LOGGER_WRITE(Logger::OUP , __LINE__,"OU invoked --- Status     :     "+std::to_string(ORDERMANAGER::GetStatus(*OrderLeg)));
        switch(ORDERMANAGER::GetStatus(*OrderLeg.get()))
        {
        case FILLED:
        {
            //AppendLog("OU invoked --- NET      :     "+std::to_string(ORDERMANAGER::GetNetQty(*OrderLeg)/ORDERMANAGER::GetBLQ(*OrderLeg)));
            Net = ORDERMANAGER::GetNetQty(*OrderLeg)/ORDERMANAGER::GetBLQ(*OrderLeg) ;
            break;
        }
        case NEW:
        {
            break ;
        }
        case REPLACED:
        {
            //StopAll(PFNumber);
            break;
        }
        case NEWREJECTED:
        {
            cout << " Handling NEWRejected Case Stopping STG "<< ORDERMANAGER::GetStatus(*OrderLeg.get()) <<endl;
            StopAll(PFNumber);
            break;
        }
        case CANCELLED:
        {
            cout << " Cancelling Order for PF "<< PFNumber <<endl;
            break;
        }
        case INFRAREJECTED:
        {


            cout << " Infra called termination for PF "<<PFNumber<<endl;
            //StopAll(PFNumber);
            break;
        }
        case MODREJECTED:
        {
            cout << " Infra called termination for PF "<<PFNumber<<endl;
            break;

        }
        }

        if(ORDERMANAGER::GetToken(*OrderLeg) == Update.Token)
        {
            if(ORDERMANAGER::GetStatus(*OrderLeg) != MODREJECTED or ORDERMANAGER::GetStatus(*OrderLeg) != CANREJECTED)
            {
                LastStatus = ORDERMANAGER::GetStatus(*OrderLeg);
            }

        }

    }

}

void SpeedTest::getInst(int PF, void * Params)
{
    boost::shared_ptr<StrategyContext> Stg (new SpeedTest(PF,Params));
    StrategyContext::RegisterStrategy(Stg);
}

void SpeedTest::OnSniffTrade(const int TOKEN, const int PRICE, const int QTY,const int FILLNO, const short BS)
{
}


void SpeedTest::onParamsUpdate(void * Params)
{
    auto InputMap = static_cast<std::map<std::string,std::string>*>(Params);

    for(std::map<std::string,std::string>::const_iterator it = InputMap->begin(); it!=InputMap->end(); it++)
    {

        Log -> LOGGER_WRITE(Logger::PARAMS , __LINE__, "Key "+ ( it->first ) +" Value "+ ( it->second ) );

        if(it->first=="TOKEN")// && isdigit(it->second.c_str()))
        {

            Token = atoi(it->second.c_str());
        }
        else if(it->first=="BUYSELL")
        {

            if(it->second=="BUY")
            {
                BS = BUY;
            }
            else
            {
                BS = SELL;
            }
        }
        else if(it->first=="UPDATETICKS")
        {

            UPDATETICK = atoi(it->second.c_str());
            cout << " Updating UPDATETICKS "<<UPDATETICK<< " NSETICKS "<< NSETICKS <<endl;
        }
        else if(it->first=="ORDERLOTS")
        {

            OrderLots = atoi(it->second.c_str());
        }
        else if(it->first=="TOTALLOTS")
        {

            TotalLots = atoi(it->second.c_str());
        }
        else if(it->first=="GATEWAYCODE")
        {

            GATEWAYCODE = atoi(it->second.c_str());
        }
        else if(it->first=="TIME")
        {

            Time = atoi(it->second.c_str());
        }
        else if(it->first=="DEPTH")
        {

            Depth = atoi(it->second.c_str());
        }
        else if(it->first=="RELEASE")
        {

            RELEASE = atoi(it->second.c_str());
        }
        else if(it->first=="PLACEFLAG")
        {

            PLACEFLAG = atoi(it->second.c_str());
        }
        else if(it->first=="T1CLIENTCODE")
        {

            T1CC = (it->second);
        }


    }


    if(ParamsCalled == false )
    {
        Log -> LOGGER_WRITE(Logger::PARAMS , __LINE__, "Setting ParamsCalled to true") ;
        ParamsCalled = true ;
    }

    //SetTimerCallback(Time) ;


}
